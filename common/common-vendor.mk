## Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    vendor/gms/common

PRODUCT_COPY_FILES += \
    vendor/gms/common/proprietary/product/etc/default-permissions/default-permissions-google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/default-permissions/default-permissions-google.xml \
    vendor/gms/common/proprietary/product/etc/default-permissions/default-permissions-mtg.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/default-permissions/default-permissions-mtg.xml \
    vendor/gms/common/proprietary/product/etc/permissions/com.google.android.dialer.support.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.dialer.support.xml \
    vendor/gms/common/proprietary/product/etc/permissions/privapp-permissions-google-product.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-google-product.xml \
    vendor/gms/common/proprietary/product/etc/security/fsverity/gms_fsverity_cert.der:$(TARGET_COPY_OUT_PRODUCT)/etc/security/fsverity/gms_fsverity_cert.der \
    vendor/gms/common/proprietary/product/etc/security/fsverity/play_store_fsi_cert.der:$(TARGET_COPY_OUT_PRODUCT)/etc/security/fsverity/play_store_fsi_cert.der \
    vendor/gms/common/proprietary/product/etc/sysconfig/d2d_cable_migration_feature.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/d2d_cable_migration_feature.xml \
    vendor/gms/common/proprietary/product/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google.xml \
    vendor/gms/common/proprietary/product/etc/sysconfig/google_build.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google_build.xml \
    vendor/gms/common/proprietary/product/etc/sysconfig/google-hiddenapi-package-allowlist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-hiddenapi-package-allowlist.xml \
    vendor/gms/common/proprietary/product/etc/sysconfig/nga.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/nga.xml \
    vendor/gms/common/proprietary/system_ext/etc/permissions/privapp-permissions-google-system-ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-google-system-ext.xml

# Product Apps
PRODUCT_PACKAGES += \
    AndroidPlatformServices \
	CalculatorGoogle \
	LatinImeGoogle \
	MarkupGoogle \
	NgaResources \
	PrebuiltDeskClockGoogle \
	SpeechServicesByGoogle \
	TrichromeLibrary64 \
    WebViewGoogle64

# Product priv-apps
PRODUCT_PACKAGES += \
    AndroidAutoStub \
    PrebuiltGmsCoreSc \
    PrebuiltGmsCoreSc_AdsDynamite \
    PrebuiltGmsCoreSc_CronetDynamite \
    PrebuiltGmsCoreSc_DynamiteLoader \
    PrebuiltGmsCoreSc_DynamiteModulesA \
    PrebuiltGmsCoreSc_DynamiteModulesC \
    PrebuiltGmsCoreSc_GoogleCertificates \
    PrebuiltGmsCoreSc_MapsDynamite \
    PrebuiltGmsCoreSc_MeasurementDynamite \
    GooglePartnerSetup \
    GoogleRestore \
    Phonesky \
    Velvet \
    WellbeingPrebuilt    

# System-ext priv-apps
PRODUCT_PACKAGES += \
	GoogleServicesFramework \
	SetupWizard

# Conditionally build Google Contacts Dialer or Message
ifeq ($(TARGET_BUILD_GOOGLE_TELEPHONY), true)
PRODUCT_PACKAGES += \
    GoogleDialer \
    GoogleDialerOverlay \
    com.google.android.dialer.support \
    GmsTelecommOverlay \
    GmsTelephonyOverlay \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    PrebuiltBugle
endif

PRODUCT_SOONG_NAMESPACES += vendor/gms/overlay
PRODUCT_PACKAGES += GmsOverlay GmsSettingsProviderOverlay GmsSetupWizardOverlay
